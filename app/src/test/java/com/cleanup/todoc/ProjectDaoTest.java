package com.cleanup.todoc;

import androidx.room.Room;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.test.platform.app.InstrumentationRegistry;

import com.cleanup.todoc.database.TodocDatabase;
import com.cleanup.todoc.database.dao.ProjectDao;
import com.cleanup.todoc.model.Project;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(RobolectricTestRunner.class)
public class ProjectDaoTest {

    private TodocDatabase mDataBase;
    private ProjectDao dao;

    private int numberOfProjects;

    @Rule
    public InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();

    @Before
    public void setup() throws Exception {
        this.mDataBase = Room.inMemoryDatabaseBuilder(InstrumentationRegistry.getInstrumentation().getTargetContext(), TodocDatabase.class).allowMainThreadQueries().build();
        dao = this.mDataBase.projectDao();

        if(LiveDataTestUtil.getOrAwaitValue(dao.getAllProjects()).size() == 0) {
            this.prepopulateProjectTable();
            numberOfProjects = LiveDataTestUtil.getOrAwaitValue(dao.getAllProjects()).size();
        }
    }

    @After
    public void closeDb() {
        if(this.mDataBase != null) {
            this.mDataBase.close();
        }
    }

    public void prepopulateProjectTable() {
        dao.insertProject(new Project(1L, "Project Tartampion", R.color.first_project));
        dao.insertProject(new Project(2L, "Project Lucidia", R.color.second_project));
        dao.insertProject(new Project(3L, "Project Circus", R.color.third_project));
    }

    @Test
    public void test_getAllProjects() throws InterruptedException {
        int expectedSize = numberOfProjects;
        List<Project> projects = LiveDataTestUtil.getOrAwaitValue(dao.getAllProjects());
        assertEquals(expectedSize, projects.size());
    }
}
