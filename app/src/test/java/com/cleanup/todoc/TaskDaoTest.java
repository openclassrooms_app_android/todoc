package com.cleanup.todoc;

import androidx.room.Room;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.test.platform.app.InstrumentationRegistry;

import com.cleanup.todoc.database.TodocDatabase;
import com.cleanup.todoc.database.dao.ProjectDao;
import com.cleanup.todoc.database.dao.TaskDao;
import com.cleanup.todoc.model.Project;
import com.cleanup.todoc.model.Task;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

@RunWith(RobolectricTestRunner.class)
public class TaskDaoTest {

    private TodocDatabase mDataBase;
    private ProjectDao projectDao;
    private TaskDao taskDao;

    private int numberOfTasks = 0;
    private List<Task> tasks;
    private Task taskTest;

    @Rule
    public InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();

    @Before
    public void setup() throws Exception {
        this.mDataBase = Room.inMemoryDatabaseBuilder(InstrumentationRegistry.getInstrumentation().getTargetContext(), TodocDatabase.class).allowMainThreadQueries().build();
        taskDao = this.mDataBase.taskDao();
        projectDao = this.mDataBase.projectDao();

        // PREPOPULATE TABLES
        if(LiveDataTestUtil.getOrAwaitValue(projectDao.getAllProjects()).size() == 0) {
            this.prepopulateProjectTable();
        }
        if(LiveDataTestUtil.getOrAwaitValue(taskDao.getAllTasks()).size() == 0) {
            this.prepopulateTaskTable();
            numberOfTasks = LiveDataTestUtil.getOrAwaitValue(taskDao.getAllTasks()).size();
        }

        // VARIABLES
        taskTest = new Task(4L, 3L, "Tâche 4", new Date().getTime());
        tasks = LiveDataTestUtil.getOrAwaitValue(taskDao.getAllTasks());
    }

    @After
    public void closeDb() throws Exception {
        if(this.mDataBase != null) {
            this.mDataBase.close();
        }
    }

    public void prepopulateProjectTable() {
        projectDao.insertProject(new Project(1L, "Project Tartampion", R.color.first_project));
        projectDao.insertProject(new Project(2L, "Project Lucidia", R.color.second_project));
        projectDao.insertProject(new Project(3L, "Project Circus", R.color.third_project));
    }

    public void prepopulateTaskTable() {
        taskDao.insertTask(new Task(1L, 1L, "Tâche 1", new Date().getTime()));
        taskDao.insertTask(new Task(2L, 2L, "Tâche 2", new Date().getTime()));
        taskDao.insertTask(new Task(3L, 3L, "Tâche 3", new Date().getTime()));
    }

    /**
     * Vérifie la récupération de toutes les tâches
     */
    @Test
    public void test_getAllTasks() {
        int expectedSize = numberOfTasks;
        assertEquals(expectedSize, tasks.size());
    }

    /**
     * Vérifie l'ajout et la suppression d'une tâche de test
     * @throws InterruptedException Lève une exception si il y a aucune LiveData !
     */
    @Test
    public void test_insertAndDeleteTask() throws InterruptedException {
        assertEquals(numberOfTasks, tasks.size());

        // ADD
        taskDao.insertTask(taskTest);
        int expectedNumberOfTasks = numberOfTasks + 1;
        assertEquals(expectedNumberOfTasks, LiveDataTestUtil.getOrAwaitValue(taskDao.getAllTasks()).size());

        // REMOVE
        taskDao.deleteTask(taskTest.getId());
        assertEquals(numberOfTasks, LiveDataTestUtil.getOrAwaitValue(taskDao.getAllTasks()).size());
        assertFalse(LiveDataTestUtil.getOrAwaitValue(taskDao.getAllTasks()).contains(taskTest));
    }

    @Test
    public void test_az_comparator() throws InterruptedException {
        List<Task> tasks = LiveDataTestUtil.getOrAwaitValue(taskDao.getAllTasks());
        Collections.sort(tasks, new Task.TaskAZComparator());

        assertEquals(tasks.get(0).getName(), "Tâche 1");
        assertEquals(tasks.get(1).getName(), "Tâche 2");
        assertEquals(tasks.get(2).getName(), "Tâche 3");
    }

    @Test
    public void test_za_comparator() throws InterruptedException {
        List<Task> tasks = LiveDataTestUtil.getOrAwaitValue(taskDao.getAllTasks());
        Collections.sort(tasks, new Task.TaskZAComparator());

        assertEquals(tasks.get(0).getName(), "Tâche 3");
        assertEquals(tasks.get(1).getName(), "Tâche 2");
        assertEquals(tasks.get(2).getName(), "Tâche 1");
    }

    @Test
    public void test_recent_comparator() throws InterruptedException {
        List<Task> tasks = LiveDataTestUtil.getOrAwaitValue(taskDao.getAllTasks());
        Collections.sort(tasks, new Task.TaskRecentComparator());

        assertEquals(tasks.get(0).getName(), "Tâche 3");
        assertEquals(tasks.get(1).getName(), "Tâche 2");
        assertEquals(tasks.get(2).getName(), "Tâche 1");
    }

    @Test
    public void test_old_comparator() throws InterruptedException {
        List<Task> tasks = LiveDataTestUtil.getOrAwaitValue(taskDao.getAllTasks());
        Collections.sort(tasks, new Task.TaskOldComparator());

        assertEquals(tasks.get(0).getName(), "Tâche 1");
        assertEquals(tasks.get(1).getName(), "Tâche 2");
        assertEquals(tasks.get(2).getName(), "Tâche 3");
    }
}
