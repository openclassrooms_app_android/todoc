package com.cleanup.todoc;

import androidx.test.espresso.ViewInteraction;
import androidx.test.rule.ActivityTestRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import com.cleanup.todoc.ui.MainActivity;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.action.ViewActions.scrollTo;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withContentDescription;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static com.cleanup.todoc.TestUtils.withRecyclerView;
import static org.hamcrest.Matchers.allOf;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @author Gaëtan HERFRAY
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class MainActivityInstrumentedTest {
    @Rule
    public ActivityTestRule<MainActivity> rule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void testNavigation_addAndRemoveTask() {
        int nbTasks = TestUtils.getCountFromRecyclerView(R.id.list_tasks);
        String testText = "ze test 9";

        // Ajout de la tâche de test
        ViewInteraction floatingActionButton = onView(
                allOf(withId(R.id.fab_add_task), withContentDescription("Ajouter une tâche"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                2),
                        isDisplayed()));
        floatingActionButton.perform(click());

        ViewInteraction appCompatEditText = onView(
                allOf(withId(R.id.txt_task_name),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.custom),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText.perform(replaceText(testText), closeSoftKeyboard());

        ViewInteraction appCompatButton = onView(
                allOf(withId(android.R.id.button1), withText("Ajouter"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.buttonPanel),
                                        0),
                                3)));
        appCompatButton.perform(scrollTo(), click());

        onView(withId(R.id.action_filter)).perform(click());
        onView(allOf(withId(R.id.title), withText(R.string.sort_oldest_first))).perform(click());

        onView(withId(R.id.list_tasks)).check(new TestUtils.RecyclerViewItemCountAssertion(nbTasks + 1));
        int lastItemPosition = TestUtils.getCountFromRecyclerView(R.id.list_tasks) - 1;
        onView(withRecyclerView(R.id.list_tasks).atPositionOnView(lastItemPosition, R.id.lbl_task_name)).check(matches(withText(testText)));

        // Suppression de la tâche de test
        onView(withRecyclerView(R.id.list_tasks).atPositionOnView(lastItemPosition, R.id.img_delete)).perform(click());
        onView(withId(R.id.list_tasks)).check(new TestUtils.RecyclerViewItemCountAssertion(nbTasks));
    }

    @Test
    public void testNavigation_sortTasks() {
        int nbTasks = TestUtils.getCountFromRecyclerView(R.id.list_tasks);
        String testText = "z9";

        // Ajout de la tâche de test
        ViewInteraction floatingActionButton = onView(
                allOf(withId(R.id.fab_add_task), withContentDescription("Ajouter une tâche"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                2),
                        isDisplayed()));
        floatingActionButton.perform(click());

        ViewInteraction appCompatEditText = onView(
                allOf(withId(R.id.txt_task_name),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.custom),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText.perform(replaceText(testText), closeSoftKeyboard());

        ViewInteraction appCompatButton = onView(
                allOf(withId(android.R.id.button1), withText("Ajouter"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.buttonPanel),
                                        0),
                                3)));
        appCompatButton.perform(scrollTo(), click());


        onView(withId(R.id.action_filter)).perform(click());
        onView(allOf(withId(R.id.title), withText(R.string.sort_alphabetical))).perform(click());
        onView(withRecyclerView(R.id.list_tasks).atPositionOnView(TestUtils.getCountFromRecyclerView(R.id.list_tasks) - 1, R.id.lbl_task_name)).check(matches(withText(testText)));

        onView(withId(R.id.action_filter)).perform(click());
        onView(allOf(withId(R.id.title), withText(R.string.sort_alphabetical_invert))).perform(click());
        onView(withRecyclerView(R.id.list_tasks).atPositionOnView(0, R.id.lbl_task_name)).check(matches(withText(testText)));

        onView(withId(R.id.action_filter)).perform(click());
        onView(allOf(withId(R.id.title), withText(R.string.sort_oldest_first))).perform(click());
        onView(withRecyclerView(R.id.list_tasks).atPositionOnView(TestUtils.getCountFromRecyclerView(R.id.list_tasks) - 1, R.id.lbl_task_name)).check(matches(withText(testText)));

        onView(withId(R.id.action_filter)).perform(click());
        onView(allOf(withId(R.id.title), withText(R.string.sort_recent_first))).perform(click());
        onView(withRecyclerView(R.id.list_tasks).atPositionOnView(0, R.id.lbl_task_name)).check(matches(withText(testText)));


        // Supppession de la tâche de test
        onView(withRecyclerView(R.id.list_tasks).atPositionOnView(0, R.id.img_delete)).perform(click());
        onView(withId(R.id.list_tasks)).check(new TestUtils.RecyclerViewItemCountAssertion(nbTasks));
    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
