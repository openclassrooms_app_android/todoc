package com.cleanup.todoc.database;

import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.room.Database;
import androidx.room.OnConflictStrategy;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import android.content.ContentValues;
import android.content.Context;
import androidx.annotation.NonNull;

import com.cleanup.todoc.R;
import com.cleanup.todoc.database.dao.ProjectDao;
import com.cleanup.todoc.database.dao.TaskDao;
import com.cleanup.todoc.model.Project;
import com.cleanup.todoc.model.Task;

@Database(entities = {Project.class, Task.class}, version = 1, exportSchema = false)
public abstract class TodocDatabase extends RoomDatabase {

    // SINGLETON
    private static volatile TodocDatabase INSTANCE;

    // DAO
    public abstract ProjectDao projectDao();
    public abstract TaskDao taskDao();

    // --- INSTANCE ---
    public static TodocDatabase getInstance(Context context) {
        if (INSTANCE == null) {
            synchronized (TodocDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            TodocDatabase.class, "MyDatabase.db")
                            .addCallback(prepopulateDatabase())
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    private static Callback prepopulateDatabase(){
        return new Callback() {

            @Override
            public void onCreate(@NonNull SupportSQLiteDatabase db) {
                super.onCreate(db);

                ContentValues firstProjectValues = new ContentValues();
                firstProjectValues.put("id", 1L);
                firstProjectValues.put("name", "Projet Tartampion");
                firstProjectValues.put("color", R.color.first_project);

                ContentValues secondProjectValues = new ContentValues();
                secondProjectValues.put("id", 2L);
                secondProjectValues.put("name", "Projet Lucidia");
                secondProjectValues.put("color", R.color.second_project);

                ContentValues thirdProjectValues = new ContentValues();
                thirdProjectValues.put("id", 3L);
                thirdProjectValues.put("name", "Projet Circus");
                thirdProjectValues.put("color", R.color.third_project);

                db.insert("Project", OnConflictStrategy.IGNORE, firstProjectValues);
                db.insert("Project", OnConflictStrategy.IGNORE, secondProjectValues);
                db.insert("Project", OnConflictStrategy.IGNORE, thirdProjectValues);
            }
        };
    }

}
